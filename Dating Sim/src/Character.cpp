#include "Character.h"
#include <graphics.h>
Character::Character(char * name)
{
    x[0]= 300;
    x[1]= 0;
    x[2]= 800;
    x[3]= 400;
    moodmeter=0;
    readimagefile("bilder/emuag.gif",x[0],x[1],x[2],x[3]);
    moodh=new char[17];
    mooda=new char[17];
    moodn=new char[17];
    moodl=new char[17];
    moods=new char[17];
    strcpy(moodh,"bilder/emhap.gif");
    strcpy(mooda,"bilder/emuag.gif");
    strcpy(moodn,"bilder/emneu.gif");
    strcpy(moodl,"bilder/emluv.gif");
    strcpy(moods,"bilder/emsad.gif");
}

bool Character::Csetmood(int mood){

    switch(mood){
        case HAPPY: readimagefile(moodh,x[0],x[1],x[2],x[3]);
            return true;
        case NORMAL: readimagefile(moodn,x[0],x[1],x[2],x[3]);
            return true;
        case ANGRY: readimagefile(mooda,x[0],x[1],x[2],x[3]);
            return true;
    }
    return false;
}


 void Character::CsetMoodMeterL(int mood){

    switch(mood){
        case HAPPY: moodmeter++; break;
        case NORMAL: break;
        case ANGRY: moodmeter--; break;
    }

    if(moodmeter<-6){
            readimagefile("bilder/emuag.gif",x[0],x[1],x[2],x[3]);
    }
    if(moodmeter>=-6&&moodmeter<-3){
            readimagefile("bilder/emsad.gif",x[0],x[1],x[2],x[3]);
    }
    if(moodmeter<=2&&moodmeter>=-2){
            readimagefile("bilder/emneu.gif",x[0],x[1],x[2],x[3]);
    }
    if(moodmeter>=3&&moodmeter<=6){
            readimagefile("bilder/emhap.gif",x[0],x[1],x[2],x[3]);
    }
    if(moodmeter>=7){
            readimagefile("bilder/emluv.gif",x[0],x[1],x[2],x[3]);
    }
 }
